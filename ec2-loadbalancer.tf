data "aws_availability_zones" "available" {
  state = "available"
}
resource "aws_elb" "lb-01" {
  name               = "lb-01"
  availability_zones = data.aws_availability_zones.available.names 
  security_groups = [aws_security_group.inbound-web-to-lb.id]
  listener {
    instance_port     = 8000
    instance_protocol = "http"
    lb_port           = 80
    lb_protocol       = "http"
  }
  health_check {
    healthy_threshold   = 2
    unhealthy_threshold = 2
    timeout             = 3
    target              = "HTTP:8000/"
    interval            = 10
  }
  instances                   = aws_instance.web[*].id
  cross_zone_load_balancing   = true
  idle_timeout                = 400
  connection_draining         = true
  connection_draining_timeout = 400
}
