# Webserver and Loadbalancer deployment on AWS EC2 (PoC/Combination of Terraform with Ansible)

- Highly available web-setup used as base for web-applications
- Can be easily extended by additional webserver-nodes
- Current setup uses two t2 instances (nginx webserver) and AWS elastic loadbalancer
- Loadbalancer uses both web instances as backends

## Prerequisites

- Terraform 0.12
- Ansible 2.9 or higher
- Ansible Provider for Terraform:
https://github.com/nbering/terraform-provider-ansible/
- Inventory Script:
https://github.com/nbering/terraform-inventory/

## Getting Started

- Set up aws id and key @~/.aws/credentials
- create generic ssh keypair for administration of newly created resources (~/.ssh/aws-generic for example)
- Clone this repo and start with `terraform init`
- Check the construction with `terraform plan` and build it with `terraform apply`
- Finalize with Ansible: 
`ansible-playbook -i /etc/ansible/terraform.py ansible-install-nginx.yml -b -u ec2-user --private-key .ssh/aws-generic`

- Another option is to use the shipped build-up scripts (scenario1-build.bash)

**Be cautious with the bash-scripts, especially scenario1-destroy.bash, since these scripts doesn't ask for your "ok" to build or destroy the setup.
Use these scripts for testing purposes.**

# Authors

* **Michel Drozdz** - *Initial work* 

# License

This project is licensed under the GNU GPLv3 License.

# Acknowledgments

- Terraform Devs and Documentation
- https://github.com/nbering/