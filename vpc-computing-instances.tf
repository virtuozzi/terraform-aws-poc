resource "aws_security_group" "inbound-ssh" {
  name        = "inbound-ssh"
  description = "Allow web and ssh inbound traffic"
  vpc_id      = data.aws_vpc.default.id # uses your default vpc populated from aws
  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port   = 8000
    to_port     = 8000
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}
