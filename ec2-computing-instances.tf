resource "aws_instance" "web" {
  ami             = var.region_ami[var.region]
  count           = var.web-instances-count
  instance_type   = var.instance_type
  key_name        = var.key_name
  security_groups = ["inbound-ssh"]
  connection {
    type        = "ssh"
    user        = "ec2-user"
    private_key = file(var.private_key_path)
    host        = self.public_ip
  }
  tags = {
    Name = var.project_name
  }
}
resource "ansible_host" "web" {
    inventory_hostname = aws_instance.web[count.index].public_dns
    count = var.web-instances-count
                groups = ["web"]
                vars = {
                    foo = "bar"
                }
}
