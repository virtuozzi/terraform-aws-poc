terraform {  
    required_providers {
       ansible = {
       source = "registry.github.com.local/nbering/ansible"
       version = "~> 1.0.3"
       }
    }
}
data "aws_vpc" "default" {
    default = true
}
resource "aws_key_pair" "aws-generic" {
  key_name   = var.key_name
  public_key = file(var.public_key_path)
}
provider "aws" {
  profile = "default"
  region  = var.region
}
