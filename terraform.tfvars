key_name = "aws-generic"
public_key_path = "~/.ssh/aws-generic.pub"
private_key_path = "~/.ssh/aws-generic"
region = "us-east-1"
region_ami = {eu-central-1 = "ami-07dfba995513840b5", us-east-1 = "ami-0c322300a1dd5dc79"} #rhel8
instance_type = "t2.micro"
web-instances-count = "2"
project_name = "java application XYv1"
cidr_incoming_lb = [
	"0.0.0.0/0", 
]
