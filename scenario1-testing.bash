#!/bin/bash
PATH=/usr/local/bin/:/usr/bin/
echo "---------------------------------------------------------------------------------------------------------"
if curl $(terraform state show aws_elb.lb-01 | grep dns_name | awk '{print $3}' | sed 's/"//g')\
 | egrep 'Welcome to CentOS|Test Page for the Nginx HTTP Server on Red Hat Enterprise Linux' &> /dev/null; then
  echo
  echo "Exit Code:" $?
  echo "Webpage and Loadbalancer up & running"
fi
echo "---------------------------------------------------------------------------------------------------------"