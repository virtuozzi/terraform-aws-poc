variable "project_name" {
    type = string
}
variable "key_name" {
    type = string
}
variable "private_key_path" {
    type = string
}
variable "public_key_path" {
    type = string
}
variable "region" {
    type = string
}
variable "instance_type" {
    type = string
}
variable "region_ami" {
    type = map
}
variable "web-instances-count" {
    type    = string
}
variable "cidr_incoming_lb" {
    type    = list
}
