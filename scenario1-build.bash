#!/bin/bash
PATH=/usr/local/bin/:/usr/bin/
terraform apply -auto-approve && sleep 120 && ansible-playbook \
-i /etc/ansible/terraform.py ansible-install-nginx.yml -b -u ec2-user --private-key .ssh/aws-generic
